import './styles/index.css';

const API_KEY: string = '383ec950eeccae2d8a89930e172d0463';
const filmsCard = document.getElementById('films');

const getFilms = async (): Promise<any> => {
  const data = await fetch(
    `https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&language=fr`
  );
  const res = await data.json();
  const { results } = res;
  console.log(results);
  filmsCard.innerHTML = '';
  results.forEach(function (e: any): any {
    filmsCard.innerHTML += `<div
    class="each mb-10 m-2 shadow-lg border-gray-800 bg-gray-100 relative"
    >
    <img
      class="w-full"
      src=${
        e.poster_path
          ? `https://image.tmdb.org/t/p/w500/${e.poster_path}`
          : 'http://via.placeholder.com/1080x1580'
      }
      alt=""
    />
    <div
      class="badge absolute top-0 right-0 bg-red-500 m-1 text-gray-200 p-1 px-2 text-xs font-bold rounded"
    >
    <a href=""><i class="fas fa-heart"></i></a>
    </div>
    <div
      class="info-box text-xs flex p-1 font-semibold text-gray-500 bg-gray-300"
    >
      <span class="mr-1 p-1 px-2 font-bold">${
        e.popularity
      }<br />Popularity</span>
      <span class="mr-1 p-1 px-2 font-bold border-l border-gray-400"
        >${e.vote_average} Vote average</span
      >
      <span class="mr-1 p-1 px-2 font-bold border-l border-gray-400"
        >${e.vote_count} Vote count</span
      >
    </div>
    <div class="desc p-4 text-gray-800">
      <a
        href="https://www.youtube.com/watch?v=dvqT-E74Qlo"
        target="_new"
        class="title font-bold block cursor-pointer hover:underline"
        >${e.original_title}</a
      >
      <a
        href="https://www.youtube.com/user/sam14319"
        target="_new"
        class="badge bg-indigo-500 text-blue-100 rounded px-1 text-xs font-bold cursor-pointer"
        >${e.release_date ? e.release_date : 'No date'}</a
      >
      <span class="description text-sm block py-2 border-gray-400 mb-2 truncate"
        >${e.overview}</span
      >
    </div>
    </div>`;
  });
};

getFilms();

const searchFilm = async (): Promise<any> => {
  const searchBar = (document.getElementById('searchBar') as HTMLInputElement)
    .value;
  const data = await fetch(
    `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=fr&query=${searchBar}`
  );
  const res = await data.json();
  const { results } = res;
  console.log(results);
  filmsCard.innerHTML = '';
  results.forEach(function (e: any): any {
    filmsCard.innerHTML += `<div
    class="each mb-10 m-2 shadow-lg border-gray-800 bg-gray-100 relative"
    >
    <img
      class="w-full"
      src=${
        e.poster_path
          ? `https://image.tmdb.org/t/p/w500/${e.poster_path}`
          : 'http://via.placeholder.com/1080x1580'
      }
      alt=""
    />
    <div
      class="badge absolute top-0 right-0 bg-red-500 m-1 text-gray-200 p-1 px-2 text-xs font-bold rounded"
    >
    <a href=""><i class="fas fa-heart"></i></a>
    </div>
    <div
      class="info-box text-xs flex p-1 font-semibold text-gray-500 bg-gray-300"
    >
      <span class="mr-1 p-1 px-2 font-bold">${
        e.popularity
      }<br />Popularity</span>
      <span class="mr-1 p-1 px-2 font-bold border-l border-gray-400"
        >${e.vote_average} Vote average</span
      >
      <span class="mr-1 p-1 px-2 font-bold border-l border-gray-400"
        >${e.vote_count} Vote count</span
      >
    </div>
    <div class="desc p-4 text-gray-800">
      <a
        href="https://www.youtube.com/watch?v=dvqT-E74Qlo"
        target="_new"
        class="title font-bold block cursor-pointer hover:underline"
        >${e.original_title}</a
      >
      <a
        href="https://www.youtube.com/user/sam14319"
        target="_new"
        class="badge bg-indigo-500 text-blue-100 rounded px-1 text-xs font-bold cursor-pointer"
        >${e.release_date ? e.release_date : 'No date'}</a
      >
      <span class="description text-sm block py-2 border-gray-400 mb-2 truncate"
        >${e.overview}</span
      >
    </div>
    </div>`;
  });
};
document.getElementById('startSearch').addEventListener('click', searchFilm);
